import { Injectable } from '@angular/core';
import { AbstractPath } from './abstract-path.class';
import { Point } from './point.class';
import { BasicPath } from './basic-path.class';

@Injectable()
export class DrawService {

  /**
   * Contains the indexes of current paths updated and showed on the screen.
   * @type {Array}
   */
  public currentUpdatingPath: number[] = [];

  /**
   * Max number of lines showed on the screen.
   */
  public maxLinesTogether: number;

  /**
   * Canvas reference.
   */
  public ctx: CanvasRenderingContext2D;

  /**
   * Array of paths (street) that contains different points to animate the lines.
   */
  private paths: AbstractPath[];

  /**
   * Frame animation reference.
   */
  private animationReference: number;

  /**
   * Sets the array of paths and generates randomly which lines to animate.
   * @param paths
   */
  public setPaths(paths: AbstractPath[]) {
    this.paths = paths;
    this.generateRandomAnimatedPaths();
  }

  public getPaths(): AbstractPath[] {
    return this.paths;
  }

  /**
   * Generates a random array of indexes to show random lines each time.
   */
  private generateRandomAnimatedPaths() {
    while (this.currentUpdatingPath.length < this.maxLinesTogether) {
      const index = AbstractPath.randomIntFromInterval(0, this.paths.length - 1);
      if (this.currentUpdatingPath.indexOf(index) === -1) {
        this.currentUpdatingPath.push(index);
        this.paths[index].started = true;
      }
    }
  }

  /**
   * Draws lines with custom properties.
   * @param points Array of Points.
   * @param lineWidth
   * @param lineCap
   * @param lineJoin
   * @param strokeStyle
   */
  public drawLine(points: Point[], lineWidth: number, lineCap: string, lineJoin: string, strokeStyle: string) {
    this.ctx.lineWidth = lineWidth;
    this.ctx.lineCap  = lineCap;
    this.ctx.lineJoin = lineJoin;
    this.ctx.strokeStyle = strokeStyle;
    this.ctx.beginPath();
    for (let i = 0; i < points.length - 1; i++) {
      this.ctx.moveTo(points[i].x, points[i].y);
      this.ctx.lineTo(points[i + 1].x, points[i + 1].y);
    }
    this.ctx.stroke();
  }

  /**
   * Draws a grid inside the canvas ( usefully to debug ).
   */
  public drawGrid(width: number, rows: number, columns: number) {
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < columns; j++) {
        this.ctx.rect(i * width, j * width, width, width);
        this.ctx.stroke();
      }
    }
  }

  /**
   * Sets some line properties e starts the animation.
   * @param lineProperties
   */
  public draw(lineProperties: any): void {
    this.ctx.lineWidth     = lineProperties.lineWidth;
    this.ctx.shadowOffsetX = lineProperties.shadowOffsetX;
    this.ctx.shadowOffsetY = lineProperties.shadowOffsetY;
    this.ctx.shadowColor   = lineProperties.shadowColor;
    this.ctx.lineCap  = lineProperties.lineCap;
    this.ctx.lineJoin = lineProperties.lineJoin;
    this.ctx.strokeStyle = lineProperties.colorLines;

    this.drawAnimation();

  }


  /**
   * Updates different lines coordinates and render on the screen, then registers again next animation loop.
   */
  private drawAnimation() {

    this.update();

    this.render();

    this.animationReference = window.requestAnimationFrame( this.drawAnimation.bind(this) );

  }

  /**
   * Clears/Stops current animation.
   */
  public resetAnimation() {
    if (this.animationReference) {
      window.cancelAnimationFrame(this.animationReference);
    }
  }

  /**
   * Updates the lines coordinates.
   */
  public update(): void {

    let generate = false;

    for (let i: number = this.currentUpdatingPath.length; i--; ) {
      this.paths[this.currentUpdatingPath[i]].update();
      if ( !this.paths[ this.currentUpdatingPath[i] ].started ) {
        this.currentUpdatingPath.splice(i, 1);
        generate = true;
      }
    }
    if (generate) {
      this.generateRandomAnimatedPaths();
    }
  }


  /**
   * Renders/Draws the current moving lines on the screen.
   */
  public render(): void {

    this.ctx.beginPath();

    for (let i = 0; i < this.currentUpdatingPath.length; i++) {

      const lines = this.paths[this.currentUpdatingPath[i]].currentCoordinates;

      const coordinates = (<BasicPath>this.paths[this.currentUpdatingPath[i]]).lastCoordinate;

      // Clearing only the lines currently updated.
      for (let j = 0; j < coordinates.length - 1; j++) {

        const from = coordinates[j]
          , to   = coordinates[j + 1];

        if (from.x !== to.x) {

          const width = Math.abs( to.x > from.x ? to.x - from.x : from.x - to.x ) + 1;
          if (to.x > from.x) {
            this.ctx.clearRect(from.x - 1, from.y - 1, width, 2);
          } else {
            this.ctx.clearRect(to.x, to.y - 1, width, 2);
          }

        } else {

          const height = Math.abs( to.y > from.y ? to.y - from.y : from.y - to.y ) + 2;
          if (to.y > from.y ) {
            this.ctx.clearRect(to.x - 1, from.y - 1, 2, height);
          } else {
            this.ctx.clearRect(from.x - 1, to.y - 1, 2, height);
          }

        }

      }

      // Drawing updated lines.
      for (let j = 0; j < lines.length; j++) {
        this.ctx.moveTo(lines[j].startPointLine.x, lines[j].startPointLine.y);
        this.ctx.lineTo(lines[j].endPointLine.x, lines[j].endPointLine.y);
      }

    }

    this.ctx.stroke();

  }

}
