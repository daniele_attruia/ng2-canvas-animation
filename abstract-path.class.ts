import { Line, Direction } from './line.class';

export abstract class AbstractPath {

  /**
   * Multiplied for the speed of lines.
   * @type {number}
   */
  private static MULTIPLIER = 2;

  /**
   * Min line width.
   * @type {number}
   */
  private static MIN_LINE_WIDTH = 50;

  /**
   * Max line width.
   * @type {number}
   */
  private static MAX_LINE_WIDTH = 130;

  public coordinates: Line[];

  /**
   * Current coordinates to update.
   * @type {Array}
   */
  public currentCoordinates: Line[] = [];

  /**
   * Index to decide which coordinate tp update and to show.
   */
  public currentCoordinatesIndex: number;

  public started = false;

  /**
   * Generates random number from min to max.
   * @param min
   * @param max
   * @returns {number}
   */
  static randomIntFromInterval(min: number, max: number) {
    return (Math.random() * (max - min + 1) + min) << 0 ;
  }

  /**
   * Generates random speed.
   * @returns {number}
   */
  private static getRandomSpeed() {

    const number: number = AbstractPath.randomIntFromInterval(1, 5);
    let speed: number;

    switch (number) {
      case 1:
        speed = 2.5 * AbstractPath.MULTIPLIER;
        break;
      case 2:
        speed = 2.8 * AbstractPath.MULTIPLIER;
        break;
      case 3:
        speed = 3.2 * AbstractPath.MULTIPLIER;
        break;
      case 4:
        speed = 3.5 * AbstractPath.MULTIPLIER;
        break;
      case 5:
        speed = 3.8 * AbstractPath.MULTIPLIER;
        break;
    }

    return speed;

  }

  constructor(coordinates: Line[]) {
    this.coordinates = coordinates;
    this.currentCoordinatesIndex = 0;
    this.init();
  }

  /**
   * Init random speed and width lines.
   */
  public init() {

    const speed: number = AbstractPath.getRandomSpeed();
    const width: number = AbstractPath.randomIntFromInterval(AbstractPath.MIN_LINE_WIDTH, AbstractPath.MAX_LINE_WIDTH);

    for (let i = 0; i < this.coordinates.length; i++ ) {

      const direction: Direction = this.coordinates[i].direction;
      this.coordinates[i].speed = direction === Direction.TOP || direction === Direction.LEFT ? -speed : speed;
      this.coordinates[i].width = width;
      this.coordinates[i].restore();

    }

  }

  /**
   * Abstract method to implement.
   */
  public abstract update(): boolean;

}
