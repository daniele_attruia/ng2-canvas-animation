import { Point } from './point.class';

export class Line {

  /**
   * Starting point of the line to move.
   */
  public startPointLine: Point;

  /**
   * Ending point of the line to move.
   */
  public endPointLine: Point;

  /**
   * Starting and ending points to restore when the animation is finished.
   * @type {Array}
   */
  public backupPoints: Point[] = [];

  /**
   * To stop the end point updating.
   */
  public stopMovingEndPoint: boolean;

  /**
   * Width of the line.
   */
  public width: number;

  /**
   * Speed of the line.
   */
  public speed: number;

  /**
   * Coordinate to move (x|y).
   */
  public coordinateToMove: string;

  /**
   * Line direction (TOP|RIGHT|BOTTOM|LEFT|END)
   */
  public direction: Direction;

  /**
   * Checks if the line fromPosition is touching the line toPosition.
   * If they are touching the from line is aligned with the toPosition coordinate.
   * @param fromDirection
   * @param fromPosition
   * @param toPosition
   * @returns {boolean}
   */
  public static lineFromIsTouchingLineTo(fromDirection: Line, fromPosition: number, toPosition: number) {

    let touch = false;

    switch (fromDirection.direction) {
      case Direction.TOP:
      case Direction.LEFT:
        touch = fromPosition <= toPosition;
        break;
      case Direction.BOTTOM:
      case Direction.RIGHT:
        touch = fromPosition >= toPosition;
        break;
    }
    if (touch) {
      fromDirection.alignEndPoint(toPosition);
    }

    return touch;

  }

  constructor(startPoint: Point, direction: Direction) {
    this.startPointLine = startPoint;
    this.endPointLine = new Point(startPoint.x, startPoint.y);

    this.backupPoints.push(new Point(startPoint.x, startPoint.y));
    this.backupPoints.push(new Point(startPoint.x, startPoint.y));

    this.direction = direction;
    this.setCoordinateToMove();
  }

  /**
   * Sets the current coordinates to move.
   */
  private setCoordinateToMove() {

    if (this.direction === Direction.TOP || this.direction === Direction.BOTTOM) {
      this.coordinateToMove = 'y';
    } else if (this.direction === Direction.RIGHT || this.direction === Direction.LEFT) {
      this.coordinateToMove = 'x';
    }

  }

  /**
   * Updates start and end points.
   */
  public update(): void {

    this.moveEndPoint();
    this.moveStartPoint();

  }

  /**
   * Updates the end point.
   */
  private moveEndPoint() {
    if ( !this.stopMovingEndPoint ) {
      (<any>this.endPointLine)[this.coordinateToMove] = ((<any>this.endPointLine)[this.coordinateToMove] + this.speed) << 0 ;
    }
  }

  /**
   * Updates the start point.
   */
  private moveStartPoint() {
    const point = Math.abs((<any>this.endPointLine)[this.coordinateToMove] - (<any>this.startPointLine)[this.coordinateToMove]);
    if (point > this.width || this.stopMovingEndPoint) {
      (<any>this.startPointLine)[this.coordinateToMove] = ( (<any>this.startPointLine)[this.coordinateToMove] + this.speed) << 0 ;
    }
  }

  /**
   * Gets the current starting point coordinate to move according to coordinateToMove string
   * @param coordinateToMove (x|y)
   * @returns {number}
   */
  public getStartPoint(coordinateToMove: string): number {
    return (<any>this.startPointLine)[coordinateToMove];
  }

  /**
   * Gets the current ending point coordinate to move according to coordinateToMove string
   * @param coordinateToMove (x|y)
   * @returns {number}
   */
  public getEndPoint(coordinateToMove: string): number {
    return (<any>this.endPointLine)[coordinateToMove];
  }

  /**
   * Sets the end point coordinate to toPosition.
   * @param toPosition
   */
  public alignEndPoint(toPosition: number) {
    (<any>this.endPointLine)[this.coordinateToMove] = toPosition;
  }

  /**
   * Restore end, start points and stoMovingEndPoint to default value.
   */
  public restore() {
    this.startPointLine = new Point(this.backupPoints[0].x, this.backupPoints[0].y);
    this.endPointLine = new Point(this.backupPoints[1].x, this.backupPoints[1].y);
    this.stopMovingEndPoint = false;
  }

}

export enum Direction {
  TOP,
  RIGHT,
  BOTTOM,
  LEFT,
  END
}
