import { AbstractPath } from './abstract-path.class';
import { Line, Direction } from './line.class';
import { Point } from './point.class';

export class BasicPath extends AbstractPath {

  public lastCoordinate: Array<Point> = [];

  /**
   * Current width of animated line.
   * @type {number}
   */
  private currentLineWidth = 0;

  constructor(coordinates: Line[]) {
    super(coordinates);
  }

  /**
   * Updates line coordinates, resets all params when the animation is finished.
   * @returns {boolean}
   */
  public update(): boolean {

    this.currentCoordinates = [];
    this.lastCoordinate = [];

    this.currentLineWidth += Math.abs(this.coordinates[this.currentCoordinatesIndex].speed);

    const update: boolean = this.currentCoordinatesIndex < (this.coordinates.length - 1);

    if (update) {
      this.updateCoordinates(this.currentCoordinatesIndex);
    } else {
      this.init();
    }

    return update;

  }

  public init() {
    super.init();
    this.currentCoordinatesIndex = 0;
    this.started = false;
  }

  /**
   * Updates coordinates pushing/removing them inside currentCoordinates array.
   * @param index
   */
  private updateCoordinates(index: number) {

    const from = this.coordinates[index];
    const to = this.coordinates[index + 1];

    from.update();

    this.currentCoordinates.push(from);

    this.lastCoordinate.push(from.backupPoints[0]);
    this.lastCoordinate.push(to.backupPoints[0]);

    // If the current line is touching the next point, it starts to animate the next line.
    if ( Line.lineFromIsTouchingLineTo(from, from.getEndPoint(from.coordinateToMove), to.getStartPoint(from.coordinateToMove) ) ) {

      from.stopMovingEndPoint = index === this.currentCoordinatesIndex && this.currentLineWidth >= from.width;

      if (to.direction !== Direction.END) {
        this.updateCoordinates(index + 1);
      }

    }

    // If the current start point of the line is touching the end point, remove it from the currentCoordinates array.
    if ( Line.lineFromIsTouchingLineTo(from, from.getStartPoint(from.coordinateToMove), from.getEndPoint(from.coordinateToMove) ) ) {
      this.currentCoordinatesIndex++;
      this.currentCoordinates.shift();
    }

  }

}
